//
//  Colors.swift
//  TapAppWidget3
//
//  Created by verec on 29/08/2016.
//  Copyright © 2016 Cantabilabs Ltd. All rights reserved.
//

import Foundation
import UIKit

struct ColorProvider {
    static let beautyfulColor = UIColor(hue: 0.9, saturation: 0.5, brightness: 0.9, alpha: 0.7)
}

public class Colors : NSObject {

    public var mainColor : UIColor {
        return ColorProvider.beautyfulColor
    }

    public var resourceTextLabel: String {
        if let bundlePath = NSBundle.mainBundle().pathForResource("Res", ofType: "text", inDirectory: "res") {
            do {
                return try NSString(contentsOfFile: bundlePath, encoding: NSUTF8StringEncoding) as String
            } catch {
            }
        }
        return ""
    }
}